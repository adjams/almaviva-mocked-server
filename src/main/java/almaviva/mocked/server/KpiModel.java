package almaviva.mocked.server;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class KpiModel {
    private final int praticeTotalNumber ;
    private final int standByPraticeNumber;
    private final int toAssignPraticeNumber;
    private	final int processingPraticeNumber;
    private	final int archivePraticeNumber;
}
