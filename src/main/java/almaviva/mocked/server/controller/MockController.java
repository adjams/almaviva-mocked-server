package almaviva.mocked.server.controller;

import almaviva.mocked.server.KpiModel;
import org.joda.time.DateTime;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.http.ResponseEntity.ok;


@RestController
@RequestMapping(value = "/mockService" ,  produces = MediaType.APPLICATION_JSON_VALUE)
public class MockController {

    @GetMapping
    public ResponseEntity<KpiModel> prova(){
        return ok(KpiModel.builder()
                .toAssignPraticeNumber(4)
                .processingPraticeNumber(3)
                .standByPraticeNumber(6)
                .archivePraticeNumber(8)
                .praticeTotalNumber(3)
                .build());
    }


    public static class Prova{
        DateTime data = new DateTime()  ;
        private String value = data.toString();
        public String getValue(){
            return data.toString();
        }
    }

    @GetMapping("ciao")
        public ResponseEntity<Prova> val(){
        List<String> list = new ArrayList<>();
        list.add("una volta");
        list.add("sono");
        list.add("uscito");
        return ok(new Prova() );
    }

    @GetMapping(value = "infoPratice")
    public ResponseEntity<String> infoPratice(
            @RequestParam("userId") String userId
    ){
        return ok(
                "{\n" +
                        "    \"clone\": \""+ userId + "\",\n" +
                        "    \"nsg\": \"some erro msg\",\n" +
                        "    \"superNDG\": \"super ndg\",\n" +
                        "    \"dateAutMople\": \"2019-05-28T06:03:20.351+0000\",\n" +
                        "    \"impieghiPortafoglio\": 3.5,\n" +
                        "    \"isRoutine\": true,\n" +
                        "    \"isTarget\": true,\n" +
                        "    \"isConcorsuale\": true,\n" +
                        "    \"nominative\": \"some nominative\",\n" +
                        "    \"propMople\": \"propMople\",\n" +
                        "    \"taxCode\": \"some taxtCode\",\n" +
                        "    \"agencyCode\": \"angencyCode00089\",\n" +
                        "    \"agencyDescription\": \"our agency do something usefull  for people\",\n" +
                        "    \"idManager\": \"agencyManager00089\",\n" +
                        "    \"idAgencyDirector\": \"agencyDirector00089\",\n" +
                        "    \"agencyDirectorName\": \"Marco pollo\",\n" +
                        "    \"textHeader\": \"some textHeader\"\n" +
                        "}"
        );
    }


    @GetMapping(value = "praticeNDG")
    public ResponseEntity<String> ndg(
            @RequestParam("idPratice") String idPratice
    ){
        return ok("[\n" +
                "    {\n" +
                "        \"type\": \"some type\",\n" +
                "        \"riskClass\": \"class risk\",\n" +
                "        \"ndg\": \"some ndg\",\n" +
                "        \"header\": \"some header\",\n" +
                "        \"qualificaPartecipazione\": \"qualifica partecipationn\",\n" +
                "        \"idNdg\": \"id of ndg1\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"type\": \"some type\",\n" +
                "        \"riskClass\": \"class risk\",\n" +
                "        \"ndg\": \"some ndg\",\n" +
                "        \"header\": \"some header\",\n" +
                "        \"qualificaPartecipazione\": \"qualifica partecipationn\",\n" +
                "        \"idNdg\": \"id of ndg2\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"type\": \"some type\",\n" +
                "        \"riskClass\": \"class risk\",\n" +
                "        \"ndg\": \"some ndg\",\n" +
                "        \"header\": \"some header\",\n" +
                "        \"qualificaPartecipazione\": \"qualifica partecipationn\",\n" +
                "        \"idNdg\": \"id of ndg3\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"type\": \"some type\",\n" +
                "        \"riskClass\": \"class risk\",\n" +
                "        \"ndg\": \"some ndg\",\n" +
                "        \"header\": \"some header\",\n" +
                "        \"qualificaPartecipazione\": \"qualifica partecipationn\",\n" +
                "        \"idNdg\": \"id of ndg4\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"type\": \"some type\",\n" +
                "        \"riskClass\": \"class risk\",\n" +
                "        \"ndg\": \"some ndg\",\n" +
                "        \"header\": \"some header\",\n" +
                "        \"qualificaPartecipazione\": \"qualifica partecipationn\",\n" +
                "        \"idNdg\": \"id of ndg5\"\n" +
                "    }\n" +
                "]");
    }


    @GetMapping(value = "praticeRapporto")
    public ResponseEntity<String> rapporto(
            @RequestParam("idPratice") String idPratice
    ){
        return ok("[\n" +
                "    {\n" +
                "        \"idRapporto\": \""+ idPratice + "\",\n" +
                "        \"agency\": \"agency\",\n" +
                "        \"abi\": \"abi\",\n" +
                "        \"nsg\": \"nsg\",\n" +
                "        \"cat\": \"cat\",\n" +
                "        \"dataApRapp\": \"2019-05-28T10:03:50.073+0000\",\n" +
                "        \"dataVarRapp\": \"2019-05-28T10:03:50.073+0000\",\n" +
                "        \"dataLape\": \"2019-05-28T10:03:50.073+0000\",\n" +
                "        \"dataEst\": \"2019-05-28T10:03:50.073+0000\",\n" +
                "        \"estRapporto\": true,\n" +
                "        \"note\": \"some note\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"idRapporto\": \"id rapporto2\",\n" +
                "        \"agency\": \"agency\",\n" +
                "        \"abi\": \"abi\",\n" +
                "        \"nsg\": \"nsg\",\n" +
                "        \"cat\": \"cat\",\n" +
                "        \"dataApRapp\": \"2019-05-28T10:03:50.073+0000\",\n" +
                "        \"dataVarRapp\": \"2019-05-28T10:03:50.073+0000\",\n" +
                "        \"dataLape\": \"2019-05-28T10:03:50.073+0000\",\n" +
                "        \"dataEst\": \"2019-05-28T10:03:50.073+0000\",\n" +
                "        \"estRapporto\": true,\n" +
                "        \"note\": \"some note\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"idRapporto\": \"id rapporto3\",\n" +
                "        \"agency\": \"agency\",\n" +
                "        \"abi\": \"abi\",\n" +
                "        \"nsg\": \"nsg\",\n" +
                "        \"cat\": \"cat\",\n" +
                "        \"dataApRapp\": \"2019-05-28T10:03:50.073+0000\",\n" +
                "        \"dataVarRapp\": \"2019-05-28T10:03:50.073+0000\",\n" +
                "        \"dataLape\": \"2019-05-28T10:03:50.073+0000\",\n" +
                "        \"dataEst\": \"2019-05-28T10:03:50.073+0000\",\n" +
                "        \"estRapporto\": true,\n" +
                "        \"note\": \"some note\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"idRapporto\": \"id rapporto4\",\n" +
                "        \"agency\": \"agency\",\n" +
                "        \"abi\": \"abi\",\n" +
                "        \"nsg\": \"nsg\",\n" +
                "        \"cat\": \"cat\",\n" +
                "        \"dataApRapp\": \"2019-05-28T10:03:50.073+0000\",\n" +
                "        \"dataVarRapp\": \"2019-05-28T10:03:50.073+0000\",\n" +
                "        \"dataLape\": \"2019-05-28T10:03:50.073+0000\",\n" +
                "        \"dataEst\": \"2019-05-28T10:03:50.073+0000\",\n" +
                "        \"estRapporto\": true,\n" +
                "        \"note\": \"some note\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"idRapporto\": \"id rapporto5\",\n" +
                "        \"agency\": \"agency\",\n" +
                "        \"abi\": \"abi\",\n" +
                "        \"nsg\": \"nsg\",\n" +
                "        \"cat\": \"cat\",\n" +
                "        \"dataApRapp\": \"2019-05-28T10:03:50.073+0000\",\n" +
                "        \"dataVarRapp\": \"2019-05-28T10:03:50.073+0000\",\n" +
                "        \"dataLape\": \"2019-05-28T10:03:50.073+0000\",\n" +
                "        \"dataEst\": \"2019-05-28T10:03:50.073+0000\",\n" +
                "        \"estRapporto\": true,\n" +
                "        \"note\": \"some note\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"idRapporto\": \"id rapporto6\",\n" +
                "        \"agency\": \"agency\",\n" +
                "        \"abi\": \"abi\",\n" +
                "        \"nsg\": \"nsg\",\n" +
                "        \"cat\": \"cat\",\n" +
                "        \"dataApRapp\": \"2019-05-28T10:03:50.073+0000\",\n" +
                "        \"dataVarRapp\": \"2019-05-28T10:03:50.073+0000\",\n" +
                "        \"dataLape\": \"2019-05-28T10:03:50.073+0000\",\n" +
                "        \"dataEst\": \"2019-05-28T10:03:50.073+0000\",\n" +
                "        \"estRapporto\": true,\n" +
                "        \"note\": \"some note\"\n" +
                "    }\n" +
                "]");
    }

    @GetMapping(value = "checkResult")
    public ResponseEntity<String> checkResult(
            @RequestParam("idPratice") String idPratice
    ){
        return ok("{\n" +
                "    \"checks\": [],\n" +
                "    \"dipendencyApplication\": []\n" +
                "}");
    }



    @PostMapping(value = "savePratice")
    public ResponseEntity<String> savePratice(
            @RequestBody Object o
     ){
        if (o == null)
            return ok("{\n" +
                    "    \"code\": \"-1\",\n" +
                    "    \"description\": \"some error\"\n" +
                    "}");

        return ok("{\n" +
                "    \"code\": \""+ o.toString() +"\",\n" +
                "    \"description\": \"OK\"\n" +
                "}");
    }


    @PostMapping(value = "generique1")
    public ResponseEntity<String> generique1(
            @RequestParam("idPratice") String value,
            @RequestBody Object o
    ){
        if (o == null)
            return ok("{\n" +
                    "    \"code\": \"-1\",\n" +
                    "    \"description\": \"some error\"\n" +
                    "}");

        return ok("{\n" +
                "    \"code\": \""+ o.toString() +"\",\n" +
                "    \"description\": \"OK\"\n" +
                "}");
    }




    @PostMapping(value = "practice_emails")
    public ResponseEntity<String> generique(
            @RequestParam("idPratice") String value,
            @RequestBody Object o
    ){


        return ok(
                "[\n" +
                "            {\n" +
                "                \"from\" : \"some value\",\n" +
                "                \"date\": \"2019-05-28T10:03:50.073+0000\",\n" +
                "                \"time\": \"some time\", \n" +
                "                \"content\": \"some content\", \n" +
                "                \"idEmail\": \"some email id\"\n" +
                "            }\n" +
                "        ]");
    }

    @PostMapping(value = "pratice_history")
    public ResponseEntity<String> PraticeHistory(
            @RequestParam("idPratice") String value,
            @RequestBody Object o
    ){


        return ok("{\n" +
                "    \"events\": [],\n" +
                "    \"historyFilters\": []\n" +
                "}");
    }


    @PostMapping(value = "pratice_notes")
    public ResponseEntity<String> getNotes(
            @RequestParam("idPratice") String value,
            @RequestBody Object o
    ){
        return ok("{\n" +
                "    \"notes\": [],\n" +
                "    \"historyFilters\": []\n" +
                "}");
    }


    @GetMapping(value = "sendMail")
    public ResponseEntity<String> sendMail(
            @RequestParam("idPratice") String idPratice,
            @RequestParam("idUser") String idUser,
            @RequestParam("emailContent") String emailContent
    ){
        return ok("{\n" +
            "    \"code\": \""+ idPratice.toString()+ " " + idUser.toString() + " " +emailContent.toString()
                +"\",\n" +
                "    \"description\": \"OK\"\n" +
                "}");
    }

    @GetMapping(value = "checkList_practice")
    public ResponseEntity<String> CheckListPractice(
            @RequestParam("idPratice") String idPratice

    ){




        return ok("{\n" +
                "    \"idPratice\":\"someValue\",\n" +
                "    \"practiceType\":\"idUser\",\n" +
                "    \"idUser\":\"idUser\",\n" +
                "    \"status\":\"status\",\n" +
                "    \"notes\":[],\n" +
                "    \"InfoSection\":\"someValue\",\n" +
                "    \"checksSection\":\"checksSection\",\n" +
                "    \"checksSection\":\"checksSection\",\n" +
                "    \"relatedRapportiSection\":\"relatedRapportiSection\"\n" +
                "}");
    }



}
