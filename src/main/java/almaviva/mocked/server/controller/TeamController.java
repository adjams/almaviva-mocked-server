package almaviva.mocked.server.controller;

import almaviva.mocked.server.Response;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/team" , produces = MediaType.APPLICATION_JSON_VALUE)
public class TeamController {

    @GetMapping(value = "addetti")
    public String getAddettoList(){
        return Response.ADDETTO_TEAM_LIST;
    }

    @PutMapping(value = "addetti/{idAddetto}")
    public String updateAddetto(@PathVariable("idAddetto") String s){
        return Response.ADDETTO_TEAM;
    }

    @GetMapping(value = "deleghe")
    public String getCapoTeamList(){
        return Response.CAPO_TEAM_LIST  ;
    }

    @PutMapping(value = "deleghe/{idCapoTeam}")
    public String updateDeleghe(@PathVariable("idCapoTeam") String s){
        return Response.CAPO_TEAM;
    }
}