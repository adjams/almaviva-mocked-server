package almaviva.mocked.server.controller;


import almaviva.mocked.server.Response;

import lombok.extern.java.Log;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import static java.util.Objects.nonNull;

@Log
@RestController
@RequestMapping(value = "/pratiche",  produces = MediaType.APPLICATION_JSON_VALUE)
public class patricaControlller {


    @PostMapping(value = "counter")
    public String getPraticheCouner(@RequestBody Object o){
        return Response.PRATICA_COUNTER;
    }

    @GetMapping(value = "{idPratica}")
    public String getInfoPractice(@PathVariable("idPratica") String s){
        return Response.PRATICE_INFO;
    }

    @GetMapping(value = "{idPratica}/step/{idStep}")
    public String getPracticeChecksResult(
            @PathVariable("idPratica") String idPratica,
            @PathVariable("idStep") String idStep
    ){
        return Response.CHECK_LIST_SINGLE;
    }

    @PutMapping(value = "{idPratica}/step/{idStep}")
    public String getPracticeChecksResult(
            @PathVariable("idPratica") String idPratica,
            @PathVariable("idStep") String idStep,
            @RequestBody Object obj
    ){
        if(nonNull(obj))
            return  Response.CHECK_LIST_SINGLE;

        return  null;
    }


    @GetMapping(value = "{idPratica}/comunicazione")
    public String getMailMetadata(
            @PathVariable("idPratica") String idPratica
    ){
        return  Response.MailMetaDataList;
    }


    @PostMapping(value = "{idPratica}/storico")
    public String getStoticoList(
            @PathVariable("idPratica") String idPratica
    ){
        return  Response.STORICO_LIST;
    }


    @PostMapping()
    public String search(
            @RequestBody Object obj
    ){
        return  Response.PRATICA_SEARCH;
    }


    @PutMapping("{praticaVoltura}/gdl")
    public String cambioGdl(
            @RequestParam ("idGdl") String idGdl
    ){
        log.info("idGdl è"  + idGdl);
        return  Response.PRATICE_INFO;
    }

    @PutMapping("{praticaVoltura}/assegnazione")
    public String cambiaAddetto(
            @RequestParam ("assegnazione") String assegnazione
    ){
        log.info("asssegnazione è"  + assegnazione);
        return  Response.PRATICE_INFO;
    }

    @PutMapping("{praticaVoltura}/stato")
    public String aggiornaStato(
            @RequestParam ("stato") String nuovoStato
    ){
        log.info("nuovoStato è"  + nuovoStato);
        return  Response.PRATICE_INFO;
    }
    @PutMapping("{idPratica}/copertina")
    public String aggiornaCopertina(
            @RequestParam ("copertina") String copertina
    ){
        log.info("copertina è"  + copertina);
        return  Response.PRATICE_INFO;
    }

}
