package almaviva.mocked.server;

public class Response {

    public final static String PRATICA_COUNTER = "{\n" +
            "  \"id\": \"string\",\n" +
            "  \"idPratica\": \"string\",\n" +
            "  \"links\": [\n" +
            "    {\n" +
            "      \"href\": \"string\",\n" +
            "      \"rel\": \"string\",\n" +
            "      \"templated\": true\n" +
            "    }\n" +
            "  ],\n" +
            "  \"praticheArchiviate\": 1,\n" +
            "  \"praticheInAttesa\": 1,\n" +
            "  \"praticheInFirma\": 1,\n" +
            "  \"praticheInLavorazione\": 1,\n" +
            "  \"praticheTotali\": 1,\n" +
            "  \"returnMessages\": {\n" +
            "    \"additionalProp1\": [\n" +
            "      {\n" +
            "        \"errorCode\": \"string\",\n" +
            "        \"links\": [\n" +
            "          {\n" +
            "            \"href\": \"string\",\n" +
            "            \"rel\": \"string\",\n" +
            "            \"templated\": true\n" +
            "          }\n" +
            "        ],\n" +
            "        \"message\": \"string\",\n" +
            "        \"messageKey\": \"string\",\n" +
            "        \"messageTitle\": \"string\",\n" +
            "        \"messagesForm\": {\n" +
            "          \"fieldName\": \"string\",\n" +
            "          \"formName\": \"string\"\n" +
            "        },\n" +
            "        \"retry\": true\n" +
            "      }\n" +
            "    ],\n" +
            "    \"additionalProp2\": [\n" +
            "      {\n" +
            "        \"errorCode\": \"string\",\n" +
            "        \"links\": [\n" +
            "          {\n" +
            "            \"href\": \"string\",\n" +
            "            \"rel\": \"string\",\n" +
            "            \"templated\": true\n" +
            "          }\n" +
            "        ],\n" +
            "        \"message\": \"string\",\n" +
            "        \"messageKey\": \"string\",\n" +
            "        \"messageTitle\": \"string\",\n" +
            "        \"messagesForm\": {\n" +
            "          \"fieldName\": \"string\",\n" +
            "          \"formName\": \"string\"\n" +
            "        },\n" +
            "        \"retry\": true\n" +
            "      }\n" +
            "    ],\n" +
            "    \"additionalProp3\": [\n" +
            "      {\n" +
            "        \"errorCode\": \"string\",\n" +
            "        \"links\": [\n" +
            "          {\n" +
            "            \"href\": \"string\",\n" +
            "            \"rel\": \"string\",\n" +
            "            \"templated\": true\n" +
            "          }\n" +
            "        ],\n" +
            "        \"message\": \"string\",\n" +
            "        \"messageKey\": \"string\",\n" +
            "        \"messageTitle\": \"string\",\n" +
            "        \"messagesForm\": {\n" +
            "          \"fieldName\": \"string\",\n" +
            "          \"formName\": \"string\"\n" +
            "        },\n" +
            "        \"retry\": true\n" +
            "      }\n" +
            "    ]\n" +
            "  }\n" +
            "}";


    public static final String PRATICE_INFO = "{\n" +
            "  \"abi\": \"string\",\n" +
            "  \"annoPropostaMople\": \"string\",\n" +
            "  \"dataMople\": \"string\",\n" +
            "  \"highRisk\": \"string\",\n" +
            "  \"id\": \"string\",\n" +
            "  \"idStato\": \"string\",\n" +
            "  \"impiegoDiPortafoglio\": \"string\",\n" +
            "  \"links\": [\n" +
            "    {\n" +
            "      \"href\": \"string\",\n" +
            "      \"rel\": \"string\",\n" +
            "      \"templated\": true\n" +
            "    }\n" +
            "  ],\n" +
            "  \"matrAssegnazione\": \"string\",\n" +
            "  \"matrCapoteam\": \"string\",\n" +
            "  \"matrDeliberante\": \"string\",\n" +
            "  \"notaDiCopertina\": \"string\",\n" +
            "  \"nsg\": \"string\",\n" +
            "  \"propostaMople\": \"string\",\n" +
            "  \"protVoltura\": \"string\",\n" +
            "  \"returnMessages\": {\n" +
            "    \"additionalProp1\": [\n" +
            "      {\n" +
            "        \"errorCode\": \"string\",\n" +
            "        \"links\": [\n" +
            "          {\n" +
            "            \"href\": \"string\",\n" +
            "            \"rel\": \"string\",\n" +
            "            \"templated\": true\n" +
            "          }\n" +
            "        ],\n" +
            "        \"message\": \"string\",\n" +
            "        \"messageKey\": \"string\",\n" +
            "        \"messageTitle\": \"string\",\n" +
            "        \"messagesForm\": {\n" +
            "          \"fieldName\": \"string\",\n" +
            "          \"formName\": \"string\"\n" +
            "        },\n" +
            "        \"retry\": true\n" +
            "      }\n" +
            "    ],\n" +
            "    \"additionalProp2\": [\n" +
            "      {\n" +
            "        \"errorCode\": \"string\",\n" +
            "        \"links\": [\n" +
            "          {\n" +
            "            \"href\": \"string\",\n" +
            "            \"rel\": \"string\",\n" +
            "            \"templated\": true\n" +
            "          }\n" +
            "        ],\n" +
            "        \"message\": \"string\",\n" +
            "        \"messageKey\": \"string\",\n" +
            "        \"messageTitle\": \"string\",\n" +
            "        \"messagesForm\": {\n" +
            "          \"fieldName\": \"string\",\n" +
            "          \"formName\": \"string\"\n" +
            "        },\n" +
            "        \"retry\": true\n" +
            "      }\n" +
            "    ],\n" +
            "    \"additionalProp3\": [\n" +
            "      {\n" +
            "        \"errorCode\": \"string\",\n" +
            "        \"links\": [\n" +
            "          {\n" +
            "            \"href\": \"string\",\n" +
            "            \"rel\": \"string\",\n" +
            "            \"templated\": true\n" +
            "          }\n" +
            "        ],\n" +
            "        \"message\": \"string\",\n" +
            "        \"messageKey\": \"string\",\n" +
            "        \"messageTitle\": \"string\",\n" +
            "        \"messagesForm\": {\n" +
            "          \"fieldName\": \"string\",\n" +
            "          \"formName\": \"string\"\n" +
            "        },\n" +
            "        \"retry\": true\n" +
            "      }\n" +
            "    ]\n" +
            "  },\n" +
            "  \"sndg\": \"string\",\n" +
            "  \"struttura\": \"string\",\n" +
            "  \"tipoPratica\": \"string\",\n" +
            "  \"userUltAgg\": \"string\"\n" +
            "}";


    public final static String CHECK_LIST_RESULT ="[\n"+
            "  {\n"+
            "    \"controllo\": \"string\",\n"+
            "    \"esito\": true,\n"+
            "    \"idPratica\": \"string\",\n"+
            "    \"idStep\": 1,\n"+
            "    \"note\": \"string\",\n"+
            "    \"tipologia\": \"string\",\n"+
            "    \"titolo\": \"string\"\n"+
            "  }\n"+
            "]";

    public final static String CHECK_LIST_SINGLE =
            "  {\n"+
            "    \"controllo\": \"string\",\n"+
            "    \"esito\": false,\n"+
            "    \"idPratica\": \"string\",\n"+
            "    \"idStep\": 1,\n"+
            "    \"note\": \"string\",\n"+
            "    \"tipologia\": \"string\",\n"+
            "    \"titolo\": \"string\"\n"+
            "  }";


    public final static String MailMetaDataList = "[\n" +
            "  {\n" +
            "    \"id\": \"string\",\n" +
            "    \"idPratica\": \"string\",\n" +
            "    \"links\": [\n" +
            "      {\n" +
            "        \"href\": \"string\",\n" +
            "        \"rel\": \"string\",\n" +
            "        \"templated\": true\n" +
            "      }\n" +
            "    ],\n" +
            "    \"mittente\": \"string\",\n" +
            "    \"oggetto\": \"string\",\n" +
            "    \"orario\": \"2019-06-07T08:28:02.601Z\",\n" +
            "    \"returnMessages\": {\n" +
            "      \"additionalProp1\": [\n" +
            "        {\n" +
            "          \"errorCode\": \"string\",\n" +
            "          \"links\": [\n" +
            "            {\n" +
            "              \"href\": \"string\",\n" +
            "              \"rel\": \"string\",\n" +
            "              \"templated\": true\n" +
            "            }\n" +
            "          ],\n" +
            "          \"message\": \"string\",\n" +
            "          \"messageKey\": \"string\",\n" +
            "          \"messageTitle\": \"string\",\n" +
            "          \"messagesForm\": {\n" +
            "            \"fieldName\": \"string\",\n" +
            "            \"formName\": \"string\"\n" +
            "          },\n" +
            "          \"retry\": true\n" +
            "        }\n" +
            "      ],\n" +
            "      \"additionalProp2\": [\n" +
            "        {\n" +
            "          \"errorCode\": \"string\",\n" +
            "          \"links\": [\n" +
            "            {\n" +
            "              \"href\": \"string\",\n" +
            "              \"rel\": \"string\",\n" +
            "              \"templated\": true\n" +
            "            }\n" +
            "          ],\n" +
            "          \"message\": \"string\",\n" +
            "          \"messageKey\": \"string\",\n" +
            "          \"messageTitle\": \"string\",\n" +
            "          \"messagesForm\": {\n" +
            "            \"fieldName\": \"string\",\n" +
            "            \"formName\": \"string\"\n" +
            "          },\n" +
            "          \"retry\": true\n" +
            "        }\n" +
            "      ],\n" +
            "      \"additionalProp3\": [\n" +
            "        {\n" +
            "          \"errorCode\": \"string\",\n" +
            "          \"links\": [\n" +
            "            {\n" +
            "              \"href\": \"string\",\n" +
            "              \"rel\": \"string\",\n" +
            "              \"templated\": true\n" +
            "            }\n" +
            "          ],\n" +
            "          \"message\": \"string\",\n" +
            "          \"messageKey\": \"string\",\n" +
            "          \"messageTitle\": \"string\",\n" +
            "          \"messagesForm\": {\n" +
            "            \"fieldName\": \"string\",\n" +
            "            \"formName\": \"string\"\n" +
            "          },\n" +
            "          \"retry\": true\n" +
            "        }\n" +
            "      ]\n" +
            "    }\n" +
            "  }\n" +
            "]";

    public  final static String STORICO_LIST = "[\n" +
            "  {\n" +
            "    \"dataOperazione\": \"2019-06-07T11:34:53.310Z\",\n" +
            "    \"id\": \"string\",\n" +
            "    \"idPratica\": \"string\",\n" +
            "    \"links\": [\n" +
            "      {\n" +
            "        \"href\": \"string\",\n" +
            "        \"rel\": \"string\",\n" +
            "        \"templated\": true\n" +
            "      }\n" +
            "    ],\n" +
            "    \"operatore\": \"string\",\n" +
            "    \"operazione\": \"string\",\n" +
            "    \"returnMessages\": {\n" +
            "      \"additionalProp1\": [\n" +
            "        {\n" +
            "          \"errorCode\": \"string\",\n" +
            "          \"links\": [\n" +
            "            {\n" +
            "              \"href\": \"string\",\n" +
            "              \"rel\": \"string\",\n" +
            "              \"templated\": true\n" +
            "            }\n" +
            "          ],\n" +
            "          \"message\": \"string\",\n" +
            "          \"messageKey\": \"string\",\n" +
            "          \"messageTitle\": \"string\",\n" +
            "          \"messagesForm\": {\n" +
            "            \"fieldName\": \"string\",\n" +
            "            \"formName\": \"string\"\n" +
            "          },\n" +
            "          \"retry\": true\n" +
            "        }\n" +
            "      ],\n" +
            "      \"additionalProp2\": [\n" +
            "        {\n" +
            "          \"errorCode\": \"string\",\n" +
            "          \"links\": [\n" +
            "            {\n" +
            "              \"href\": \"string\",\n" +
            "              \"rel\": \"string\",\n" +
            "              \"templated\": true\n" +
            "            }\n" +
            "          ],\n" +
            "          \"message\": \"string\",\n" +
            "          \"messageKey\": \"string\",\n" +
            "          \"messageTitle\": \"string\",\n" +
            "          \"messagesForm\": {\n" +
            "            \"fieldName\": \"string\",\n" +
            "            \"formName\": \"string\"\n" +
            "          },\n" +
            "          \"retry\": true\n" +
            "        }\n" +
            "      ],\n" +
            "      \"additionalProp3\": [\n" +
            "        {\n" +
            "          \"errorCode\": \"string\",\n" +
            "          \"links\": [\n" +
            "            {\n" +
            "              \"href\": \"string\",\n" +
            "              \"rel\": \"string\",\n" +
            "              \"templated\": true\n" +
            "            }\n" +
            "          ],\n" +
            "          \"message\": \"string\",\n" +
            "          \"messageKey\": \"string\",\n" +
            "          \"messageTitle\": \"string\",\n" +
            "          \"messagesForm\": {\n" +
            "            \"fieldName\": \"string\",\n" +
            "            \"formName\": \"string\"\n" +
            "          },\n" +
            "          \"retry\": true\n" +
            "        }\n" +
            "      ]\n" +
            "    },\n" +
            "    \"stato\": \"string\"\n" +
            "  }\n" +
            "]";


    public final static String PRATICA_SEARCH = "{\n"+
            "  \"content\": [\n"+
            "    {\n"+
            "      \"abi\": \"string\",\n"+
            "      \"annoPropostaMople\": \"string\",\n"+
            "      \"dataMople\": \"string\",\n"+
            "      \"highRisk\": \"string\",\n"+
            "      \"id\": \"string\",\n"+
            "      \"idStato\": \"string\",\n"+
            "      \"impiegoDiPortafoglio\": \"string\",\n"+
            "      \"links\": [\n"+
            "        {\n"+
            "          \"href\": \"string\",\n"+
            "          \"rel\": \"string\",\n"+
            "          \"templated\": true\n"+
            "        }\n"+
            "      ],\n"+
            "      \"matrAssegnazione\": \"string\",\n"+
            "      \"matrCapoteam\": \"string\",\n"+
            "      \"matrDeliberante\": \"string\",\n"+
            "      \"notaDiCopertina\": \"string\",\n"+
            "      \"nsg\": \"string\",\n"+
            "      \"propostaMople\": \"string\",\n"+
            "      \"protVoltura\": \"string\",\n"+
            "      \"returnMessages\": {\n"+
            "        \"additionalProp1\": [\n"+
            "          {\n"+
            "            \"errorCode\": \"string\",\n"+
            "            \"links\": [\n"+
            "              {\n"+
            "                \"href\": \"string\",\n"+
            "                \"rel\": \"string\",\n"+
            "                \"templated\": true\n"+
            "              }\n"+
            "            ],\n"+
            "            \"message\": \"string\",\n"+
            "            \"messageKey\": \"string\",\n"+
            "            \"messageTitle\": \"string\",\n"+
            "            \"messagesForm\": {\n"+
            "              \"fieldName\": \"string\",\n"+
            "              \"formName\": \"string\"\n"+
            "            },\n"+
            "            \"retry\": true\n"+
            "          }\n"+
            "        ],\n"+
            "        \"additionalProp2\": [\n"+
            "          {\n"+
            "            \"errorCode\": \"string\",\n"+
            "            \"links\": [\n"+
            "              {\n"+
            "                \"href\": \"string\",\n"+
            "                \"rel\": \"string\",\n"+
            "                \"templated\": true\n"+
            "              }\n"+
            "            ],\n"+
            "            \"message\": \"string\",\n"+
            "            \"messageKey\": \"string\",\n"+
            "            \"messageTitle\": \"string\",\n"+
            "            \"messagesForm\": {\n"+
            "              \"fieldName\": \"string\",\n"+
            "              \"formName\": \"string\"\n"+
            "            },\n"+
            "            \"retry\": true\n"+
            "          }\n"+
            "        ],\n"+
            "        \"additionalProp3\": [\n"+
            "          {\n"+
            "            \"errorCode\": \"string\",\n"+
            "            \"links\": [\n"+
            "              {\n"+
            "                \"href\": \"string\",\n"+
            "                \"rel\": \"string\",\n"+
            "                \"templated\": true\n"+
            "              }\n"+
            "            ],\n"+
            "            \"message\": \"string\",\n"+
            "            \"messageKey\": \"string\",\n"+
            "            \"messageTitle\": \"string\",\n"+
            "            \"messagesForm\": {\n"+
            "              \"fieldName\": \"string\",\n"+
            "              \"formName\": \"string\"\n"+
            "            },\n"+
            "            \"retry\": true\n"+
            "          }\n"+
            "        ]\n"+
            "      },\n"+
            "      \"sndg\": \"string\",\n"+
            "      \"struttura\": \"string\",\n"+
            "      \"timestampCancellazione\": \"2019-06-07T16:17:46.009Z\",\n"+
            "      \"timestampFermo_posta\": \"2019-06-07T16:17:46.009Z\",\n"+
            "      \"timestampInserimento\": \"2019-06-07T16:17:46.009Z\",\n"+
            "      \"tipoPratica\": \"string\",\n"+
            "      \"tmsAgg\": \"2019-06-07T16:17:46.009Z\",\n"+
            "      \"tmsUltAggLegacy\": \"2019-06-07T16:17:46.009Z\",\n"+
            "      \"userUltAgg\": \"string\"\n"+
            "    }\n"+
            "  ],\n"+
            "  \"links\": [\n"+
            "    {\n"+
            "      \"href\": \"string\",\n"+
            "      \"rel\": \"string\",\n"+
            "      \"templated\": true\n"+
            "    }\n"+
            "  ],\n"+
            "  \"page\": {\n"+
            "    \"number\": 0,\n"+
            "    \"size\": 0,\n"+
            "    \"totalElements\": 0,\n"+
            "    \"totalPages\": 0\n"+
            "  }\n"+
            "}";



    public static  final String ADDETTO_TEAM_LIST = "[\n" +
            "  {\n" +
            "    \"assegnazioneAutomatica\": true,\n" +
            "    \"delega\": \"string\",\n" +
            "    \"descrizione\": \"string\",\n" +
            "    \"id\": \"string\",\n" +
            "    \"links\": [\n" +
            "      {\n" +
            "        \"href\": \"string\",\n" +
            "        \"rel\": \"string\",\n" +
            "        \"templated\": true\n" +
            "      }\n" +
            "    ],\n" +
            "    \"returnMessages\": {\n" +
            "      \"additionalProp1\": [\n" +
            "        {\n" +
            "          \"errorCode\": \"string\",\n" +
            "          \"links\": [\n" +
            "            {\n" +
            "              \"href\": \"string\",\n" +
            "              \"rel\": \"string\",\n" +
            "              \"templated\": true\n" +
            "            }\n" +
            "          ],\n" +
            "          \"message\": \"string\",\n" +
            "          \"messageKey\": \"string\",\n" +
            "          \"messageTitle\": \"string\",\n" +
            "          \"messagesForm\": {\n" +
            "            \"fieldName\": \"string\",\n" +
            "            \"formName\": \"string\"\n" +
            "          },\n" +
            "          \"retry\": true\n" +
            "        }\n" +
            "      ],\n" +
            "      \"additionalProp2\": [\n" +
            "        {\n" +
            "          \"errorCode\": \"string\",\n" +
            "          \"links\": [\n" +
            "            {\n" +
            "              \"href\": \"string\",\n" +
            "              \"rel\": \"string\",\n" +
            "              \"templated\": true\n" +
            "            }\n" +
            "          ],\n" +
            "          \"message\": \"string\",\n" +
            "          \"messageKey\": \"string\",\n" +
            "          \"messageTitle\": \"string\",\n" +
            "          \"messagesForm\": {\n" +
            "            \"fieldName\": \"string\",\n" +
            "            \"formName\": \"string\"\n" +
            "          },\n" +
            "          \"retry\": true\n" +
            "        }\n" +
            "      ],\n" +
            "      \"additionalProp3\": [\n" +
            "        {\n" +
            "          \"errorCode\": \"string\",\n" +
            "          \"links\": [\n" +
            "            {\n" +
            "              \"href\": \"string\",\n" +
            "              \"rel\": \"string\",\n" +
            "              \"templated\": true\n" +
            "            }\n" +
            "          ],\n" +
            "          \"message\": \"string\",\n" +
            "          \"messageKey\": \"string\",\n" +
            "          \"messageTitle\": \"string\",\n" +
            "          \"messagesForm\": {\n" +
            "            \"fieldName\": \"string\",\n" +
            "            \"formName\": \"string\"\n" +
            "          },\n" +
            "          \"retry\": true\n" +
            "        }\n" +
            "      ]\n" +
            "    }\n" +
            "  }\n" +
            "]";


    public static  final String ADDETTO_TEAM = "{\n" +
            "    \"assegnazioneAutomatica\": true,\n" +
            "    \"delega\": \"string\",\n" +
            "    \"descrizione\": \"string\",\n" +
            "    \"id\": \"string\",\n" +
            "    \"links\": [\n" +
            "      {\n" +
            "        \"href\": \"string\",\n" +
            "        \"rel\": \"string\",\n" +
            "        \"templated\": true\n" +
            "      }\n" +
            "    ],\n" +
            "    \"returnMessages\": {\n" +
            "      \"additionalProp1\": [\n" +
            "        {\n" +
            "          \"errorCode\": \"string\",\n" +
            "          \"links\": [\n" +
            "            {\n" +
            "              \"href\": \"string\",\n" +
            "              \"rel\": \"string\",\n" +
            "              \"templated\": true\n" +
            "            }\n" +
            "          ],\n" +
            "          \"message\": \"string\",\n" +
            "          \"messageKey\": \"string\",\n" +
            "          \"messageTitle\": \"string\",\n" +
            "          \"messagesForm\": {\n" +
            "            \"fieldName\": \"string\",\n" +
            "            \"formName\": \"string\"\n" +
            "          },\n" +
            "          \"retry\": true\n" +
            "        }\n" +
            "      ],\n" +
            "      \"additionalProp2\": [\n" +
            "        {\n" +
            "          \"errorCode\": \"string\",\n" +
            "          \"links\": [\n" +
            "            {\n" +
            "              \"href\": \"string\",\n" +
            "              \"rel\": \"string\",\n" +
            "              \"templated\": true\n" +
            "            }\n" +
            "          ],\n" +
            "          \"message\": \"string\",\n" +
            "          \"messageKey\": \"string\",\n" +
            "          \"messageTitle\": \"string\",\n" +
            "          \"messagesForm\": {\n" +
            "            \"fieldName\": \"string\",\n" +
            "            \"formName\": \"string\"\n" +
            "          },\n" +
            "          \"retry\": true\n" +
            "        }\n" +
            "      ],\n" +
            "      \"additionalProp3\": [\n" +
            "        {\n" +
            "          \"errorCode\": \"string\",\n" +
            "          \"links\": [\n" +
            "            {\n" +
            "              \"href\": \"string\",\n" +
            "              \"rel\": \"string\",\n" +
            "              \"templated\": true\n" +
            "            }\n" +
            "          ],\n" +
            "          \"message\": \"string\",\n" +
            "          \"messageKey\": \"string\",\n" +
            "          \"messageTitle\": \"string\",\n" +
            "          \"messagesForm\": {\n" +
            "            \"fieldName\": \"string\",\n" +
            "            \"formName\": \"string\"\n" +
            "          },\n" +
            "          \"retry\": true\n" +
            "        }\n" +
            "      ]\n" +
            "    }\n" +
            "  }";


    public  static final String CAPO_TEAM_LIST = "[\n"+
            "  {\n"+
            "    \"assegnazioneAutomatica\": true,\n"+
            "    \"delega\": [\n"+
            "      \"string\"\n"+
            "    ],\n"+
            "    \"id\": \"string\",\n"+
            "    \"links\": [\n"+
            "      {\n"+
            "        \"href\": \"string\",\n"+
            "        \"rel\": \"string\",\n"+
            "        \"templated\": true\n"+
            "      }\n"+
            "    ],\n"+
            "    \"returnMessages\": {\n"+
            "      \"additionalProp1\": [\n"+
            "        {\n"+
            "          \"errorCode\": \"string\",\n"+
            "          \"links\": [\n"+
            "            {\n"+
            "              \"href\": \"string\",\n"+
            "              \"rel\": \"string\",\n"+
            "              \"templated\": true\n"+
            "            }\n"+
            "          ],\n"+
            "          \"message\": \"string\",\n"+
            "          \"messageKey\": \"string\",\n"+
            "          \"messageTitle\": \"string\",\n"+
            "          \"messagesForm\": {\n"+
            "            \"fieldName\": \"string\",\n"+
            "            \"formName\": \"string\"\n"+
            "          },\n"+
            "          \"retry\": true\n"+
            "        }\n"+
            "      ],\n"+
            "      \"additionalProp2\": [\n"+
            "        {\n"+
            "          \"errorCode\": \"string\",\n"+
            "          \"links\": [\n"+
            "            {\n"+
            "              \"href\": \"string\",\n"+
            "              \"rel\": \"string\",\n"+
            "              \"templated\": true\n"+
            "            }\n"+
            "          ],\n"+
            "          \"message\": \"string\",\n"+
            "          \"messageKey\": \"string\",\n"+
            "          \"messageTitle\": \"string\",\n"+
            "          \"messagesForm\": {\n"+
            "            \"fieldName\": \"string\",\n"+
            "            \"formName\": \"string\"\n"+
            "          },\n"+
            "          \"retry\": true\n"+
            "        }\n"+
            "      ],\n"+
            "      \"additionalProp3\": [\n"+
            "        {\n"+
            "          \"errorCode\": \"string\",\n"+
            "          \"links\": [\n"+
            "            {\n"+
            "              \"href\": \"string\",\n"+
            "              \"rel\": \"string\",\n"+
            "              \"templated\": true\n"+
            "            }\n"+
            "          ],\n"+
            "          \"message\": \"string\",\n"+
            "          \"messageKey\": \"string\",\n"+
            "          \"messageTitle\": \"string\",\n"+
            "          \"messagesForm\": {\n"+
            "            \"fieldName\": \"string\",\n"+
            "            \"formName\": \"string\"\n"+
            "          },\n"+
            "          \"retry\": true\n"+
            "        }\n"+
            "      ]\n"+
            "    }\n"+
            "  }\n"+
            "]";

    public  static final String CAPO_TEAM = "{\n" +
            "  \"assegnazioneAutomatica\": true,\n" +
            "  \"delega\": [\n" +
            "    \"string\"\n" +
            "  ],\n" +
            "  \"id\": \"string\",\n" +
            "  \"links\": [\n" +
            "    {\n" +
            "      \"href\": \"string\",\n" +
            "      \"rel\": \"string\",\n" +
            "      \"templated\": true\n" +
            "    }\n" +
            "  ],\n" +
            "  \"returnMessages\": {\n" +
            "    \"additionalProp1\": [\n" +
            "      {\n" +
            "        \"errorCode\": \"string\",\n" +
            "        \"links\": [\n" +
            "          {\n" +
            "            \"href\": \"string\",\n" +
            "            \"rel\": \"string\",\n" +
            "            \"templated\": true\n" +
            "          }\n" +
            "        ],\n" +
            "        \"message\": \"string\",\n" +
            "        \"messageKey\": \"string\",\n" +
            "        \"messageTitle\": \"string\",\n" +
            "        \"messagesForm\": {\n" +
            "          \"fieldName\": \"string\",\n" +
            "          \"formName\": \"string\"\n" +
            "        },\n" +
            "        \"retry\": true\n" +
            "      }\n" +
            "    ],\n" +
            "    \"additionalProp2\": [\n" +
            "      {\n" +
            "        \"errorCode\": \"string\",\n" +
            "        \"links\": [\n" +
            "          {\n" +
            "            \"href\": \"string\",\n" +
            "            \"rel\": \"string\",\n" +
            "            \"templated\": true\n" +
            "          }\n" +
            "        ],\n" +
            "        \"message\": \"string\",\n" +
            "        \"messageKey\": \"string\",\n" +
            "        \"messageTitle\": \"string\",\n" +
            "        \"messagesForm\": {\n" +
            "          \"fieldName\": \"string\",\n" +
            "          \"formName\": \"string\"\n" +
            "        },\n" +
            "        \"retry\": true\n" +
            "      }\n" +
            "    ],\n" +
            "    \"additionalProp3\": [\n" +
            "      {\n" +
            "        \"errorCode\": \"string\",\n" +
            "        \"links\": [\n" +
            "          {\n" +
            "            \"href\": \"string\",\n" +
            "            \"rel\": \"string\",\n" +
            "            \"templated\": true\n" +
            "          }\n" +
            "        ],\n" +
            "        \"message\": \"string\",\n" +
            "        \"messageKey\": \"string\",\n" +
            "        \"messageTitle\": \"string\",\n" +
            "        \"messagesForm\": {\n" +
            "          \"fieldName\": \"string\",\n" +
            "          \"formName\": \"string\"\n" +
            "        },\n" +
            "        \"retry\": true\n" +
            "      }\n" +
            "    ]\n" +
            "  }\n" +
            "}";
}
