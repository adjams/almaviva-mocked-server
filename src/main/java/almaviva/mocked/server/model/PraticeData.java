package almaviva.mocked.server.model;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Builder
@Data
public class PraticeData {
    public static class PraticeDataBuilder{}
    private final String idPractice;
    private final String headerText;
    private final boolean isConcorsuale;
    private final Date dateAutMople;
}